package main;

import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

//napoveda
public class HelpController {

    @FXML
    WebView webView;
    public void initialize(){
        onLoadHelpFile();
    }
    @FXML
    public void onLoadHelpFile() {
        WebEngine webEngine = webView.getEngine();
        webEngine.load(getClass().getResource("/resources/help.html").toExternalForm());
    }
}