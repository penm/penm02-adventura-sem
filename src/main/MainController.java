package main;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.util.Callback;
import logika.*;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

import java.util.HashMap;
import java.util.Map;

public class MainController implements Observer {

    @FXML
    public TextField vstup;
    @FXML
    public TextArea vystup;
    @FXML
    public Button odesli;
    @FXML
    public ListView seznamVychodu;
    @FXML
    public ListView seznamVeci;
    @FXML
    public ImageView hrac;
    @FXML
    public ComboBox<String> kill;


    private IHra hra;
    private Map<String, Point2D> souradniceProstoru;
    private Map<String, String> ikony;

    /**
     * Metoda čte příkaz z TextFieldu a zpracovává ho
     *
     * @param actionEvent
     */
    public void odeslaniPrikazu(ActionEvent actionEvent) {
        zpracovaniPrikazu(vstup.getText());
        vstup.setText("");
    }

    public void zobrazeniPrikazu(MouseEvent mouseEvent){
        kill.getButtonCell();
    }

    /**
     * Metoda příkaz předává hře a výsledek vypisuje do TextArea
     *
     * @param prikaz Příkaz ke zpracování
     */
    private void zpracovaniPrikazu(String prikaz) {
        String odpoved = hra.zpracujPrikaz(prikaz);
        vystup.appendText("\n\nPříkaz: " + prikaz + "\n");
        vystup.appendText(odpoved + "\n");

        isKonecHry();
    }

    /**
     * Metoda zjišťuje, zda nastal konec hry.
     * V případě, že ano, deaktivuje ovládací prvky a vrátí epilog.
     */
    private void isKonecHry() {
        if (hra.konecHry()) {
            vstup.setDisable(true);
            odesli.setDisable(true);
            seznamVychodu.setDisable(true);
            vystup.appendText("\n\n" + hra.vratEpilog());
        }
    }

    /**pohyb postavy po mape**/
    private void vytvorMapuSouradnic() {
        souradniceProstoru = new HashMap<>();

        souradniceProstoru.put("Tmavá miestnosť", new Point2D(32.0, 37.0));
        souradniceProstoru.put("Chodba", new Point2D(197.0, 38.0));
        souradniceProstoru.put("Pracovňa", new Point2D(329.0, 117.0));
        souradniceProstoru.put("Pivnica", new Point2D(326.0, 37.0));
        souradniceProstoru.put("Sklad", new Point2D(199.0, 110.0));
        souradniceProstoru.put("Tunel", new Point2D(55.0, 96.0));
        souradniceProstoru.put("Dvere", new Point2D(139.0, 162.0));
    }

    /**
     * metoda na nastavenie ikon predmetov
     */

    private void vytvorZoznamIkon()
    {
        ikony = new HashMap<>();

        ikony.put("Motorka", "/resources/motorka.png");
        ikony.put("Kamarát", "/resources/osoba.png");
        ikony.put("Počítač", "/resources/pc.png");
        ikony.put("Kľúč", "/resources/kluc.png");
        ikony.put("KnihaProgramovanieVJave", "/resources/kniha.png");
    }

    @Override
    public void update() {
        ObservableList items = seznamVychodu.getItems();
        ObservableList items2 = seznamVeci.getItems();
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();
        Batoh batoh = hra.getHerniPlan().getBatoh();
        items.clear();
        items2.clear();
        items.addAll(aktualniProstor.getVychody());

        seznamVeci.setCellFactory(listView -> new ListCell<Vec>() {
            private ImageView ikonaView = new ImageView();

            @Override
            public void updateItem(Vec vec, boolean empty) {
                super.updateItem(vec, empty);
                if (empty) {
                    setText(null);
                    setGraphic(null);
                }
                else {
                    Image ikona = new Image(ikony.get(vec.getNazev()));
                    ikonaView.setSmooth(true);
                    ikonaView.setFitHeight(30);
                    ikonaView.setFitWidth(30);
                    ikonaView.setImage(ikona);
                    setText(vec.getNazev());
                    setGraphic(ikonaView);
                }
            }

        });

        items2.addAll(batoh.getSeznamVeci());

        hrac.setX(souradniceProstoru.get(aktualniProstor.getNazev()).getX());
        hrac.setY(souradniceProstoru.get(aktualniProstor.getNazev()).getY());
    }

    /**
     * Metoda načte vybraný prvek ze seznamu sousedních místností
     * a sestaví příkaz.
     * @param mouseEvent
     */
    public void pruchod(MouseEvent mouseEvent) {
        Prostor prostor = (Prostor) seznamVychodu.getSelectionModel().getSelectedItem();

        String prikaz = PrikazJdi.getNazevStatic() + Hra.getOddelovac() + prostor.getNazev();
        zpracovaniPrikazu(prikaz);
    }



    /**
     * Metoda, ktora sa pusta na zaciatku hry aby zaregistrovala herny plan podla navrhoveho vzoru observer.
     */
    public void inicializuj(IHra hra) {
        this.hra = hra;
        vystup.clear();
        vystup.appendText(hra.vratUvitani());
        vystup.setEditable(false);
        vstup.requestFocus();

        vytvorMapuSouradnic();
        vytvorZoznamIkon();

        ObservableList<String> options =
                FXCollections.observableArrayList(
                        "Option 1",
                        "Option 2",
                        "Option 3"
                );
        kill = new ComboBox<>(options);
        kill.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

            }
        });
       // kill.setItems(FXCollections.observableArrayList(new PrikazJdi(hra.getHerniPlan(),(Hra) hra)));

        hra.getHerniPlan().register( this);
        update();
    }

    /**
     * Metoda spusta novu hru v triede start
     */


    public void startNew() {
        Start.newHra();
    }

    /**
     * Metoda na spustenie nápovedy
     */


    public void openHelp() {
        Start.startHelp();
    }



}
