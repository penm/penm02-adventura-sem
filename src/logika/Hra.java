package logika;

import java.util.*;

/**
 *  Třída Hra - třída představující logiku adventury.
 * 
 *  Toto je hlavní třída  logiky aplikace.  Tato třída vytváří instanci třídy HerniPlan, která inicializuje mistnosti hry
 *  a vytváří seznam platných příkazů a instance tříd provádějící jednotlivé příkazy.
 *  Vypisuje uvítací a ukončovací text hry.
 *  Také vyhodnocuje jednotlivé příkazy zadané uživatelem.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */

public class Hra implements IHra {
    private SeznamPrikazu platnePrikazy;    // obsahuje seznam přípustných příkazů
    private HerniPlan herniPlan;
    private boolean konecHry = false;
    private ArrayList<String> pouzitePrikazy;
    private Batoh batoh;
    private static final String ODDELOVAC = " ";



    /**
     *  Vytváří hru a inicializuje místnosti (prostřednictvím třídy HerniPlan) a seznam platných příkazů.
     */
    public Hra() {
        //batoh = batoh.getInstance();
        batoh = batoh.getInstance();
        herniPlan = new HerniPlan(batoh);
        platnePrikazy = new SeznamPrikazu();
        platnePrikazy.vlozPrikaz(new PrikazNapoveda(platnePrikazy));
        platnePrikazy.vlozPrikaz(new PrikazJdi(herniPlan, this));
        platnePrikazy.vlozPrikaz(new PrikazKonec(this));
        platnePrikazy.vlozPrikaz(new PrikazUloz(this));
        platnePrikazy.vlozPrikaz(new PrikazNacti(this));
        platnePrikazy.vlozPrikaz(new PrikazSeber(herniPlan,batoh));
        platnePrikazy.vlozPrikaz(new PrikazBatoh(batoh));
        platnePrikazy.vlozPrikaz(new PrikazOdemkni(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazZabi(herniPlan));
        pouzitePrikazy = new ArrayList<>();
    }

    /**
     *  Vrátí úvodní zprávu pro hráče.
     */
    public String vratUvitani() {               
        return "Vitajte!\n" +
               "So svojím najlepším priateľom ste sa rozhodli osláviť jeho narodeniny v luxusnom podniku. Keď ste sa večer vracali domov v tmavej uličke oproti vám zastavila čierna dodávka a muži v kuklách vás uniesli do neznámeho komplexu. Vašou úlohou je prežiť a vyslobodiť sa zo zajatia.\n" +
               "Napíšte 'nápoveda', ak si neviete rady, ako hrať ďalej.\n" +
               "\n" +
               herniPlan.getAktualniProstor().dlouhyPopis();
    }
    
    /**
     * vrati uvitanie po načitani hry
     */
    public String vratUvitaniPoNacteni() {               
        return herniPlan.getAktualniProstor().dlouhyPopis();
    }
    
    
    
    /**
     *  Vrátí závěrečnou zprávu pro hráče.
     */
    public String vratEpilog() {
        return "Ďakujem že ste si zahrali túto hru. Ahoj !";
    }
    
    /** 
     * Vrací true, pokud hra skončila.
     */
     public boolean konecHry() {
        return konecHry;
    }

    /**
     *  Metoda zpracuje řetězec uvedený jako parametr, rozdělí ho na slovo příkazu a další parametry.
     *  Pak otestuje zda příkaz je klíčovým slovem  např. jdi.
     *  Pokud ano spustí samotné provádění příkazu.
     *
     *@param  radek  text, který zadal uživatel jako příkaz do hry.
     *@return          vrací se řetězec, který se má vypsat na obrazovku
     */
     public String zpracujPrikaz(String radek) {
        String [] slova = radek.split("[ \t]+");
        String slovoPrikazu = slova[0];
        String []parametry = new String[slova.length-1];
        
        
        for(int i=0 ;i<parametry.length;i++){
            parametry[i]= slova[i+1];   
        }
        String textKVypsani=" .... ";
        if (platnePrikazy.jePlatnyPrikaz(slovoPrikazu)) {
            IPrikaz prikaz = platnePrikazy.vratPrikaz(slovoPrikazu);
            textKVypsani = prikaz.provedPrikaz(parametry);
            
        }
        else {
            textKVypsani="Neviem čo tím myslíš? Tento príkaz nepoznám. ";
        }
        pouzitePrikazy.add(radek);
        return textKVypsani;
    }
    
    
     /**
     *  Nastaví, že je konec hry, metodu využívá třída PrikazKonec,
     *  mohou ji použít i další implementace rozhraní Prikaz.
     *  
     *  @param  konecHry  hodnota false= konec hry, true = hra pokračuje
     */
    void setKonecHry(boolean konecHry) {
        this.konecHry = konecHry;
    }
    
    public ArrayList<String> getPouzitePrikazy() {
        return pouzitePrikazy;
    }
    
     /**
     *  Metoda vrátí odkaz na herní plán, je využita hlavně v testech,
     *  kde se jejím prostřednictvím získává aktualní místnost hry.
     *  
     *  @return     odkaz na herní plán
     */
     public HerniPlan getHerniPlan(){
        return herniPlan;
     }

    public static String getOddelovac() {
        return ODDELOVAC;
    }

    public Batoh getBatoh(){
         return batoh;
    }

    public SeznamPrikazu getPlatnePrikazy(){
         return platnePrikazy;
    }

}

