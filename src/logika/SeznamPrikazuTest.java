package logika;


import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída SeznamPrikazuTest slouží ke komplexnímu otestování třídy  
 * SeznamPrikazu
 * 
 * @author    Luboš Pavlíček
 * @version   pro školní rok 2016/2017
 */
public class SeznamPrikazuTest
{
    private PrikazKonec prKonec;
    private PrikazJdi prJdi;
    private PrikazUloz prUloz;
    private PrikazNacti prNacti;
    private PrikazSeber prSeber;
    private PrikazBatoh prBatoh;
    private PrikazOdemkni prOdemkni;
    private PrikazZabi prZabi;
    private HerniPlan herniPlan;
    private Batoh batoh;
    
    @Before
    public void setUp() {
        Hra hra = new Hra();
        prKonec = new PrikazKonec(hra);
        prJdi = new PrikazJdi(hra.getHerniPlan(), hra);
        prUloz = new PrikazUloz(hra);
        prNacti = new PrikazNacti(hra);
        prSeber = new PrikazSeber(herniPlan,batoh);
        prBatoh = new PrikazBatoh(batoh);
        prOdemkni = new PrikazOdemkni(herniPlan);
        prZabi = new PrikazZabi(herniPlan);

    }

    @Test
    public void testVlozeniVybrani() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prUloz);
        seznPrikazu.vlozPrikaz(prNacti);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prBatoh);
        seznPrikazu.vlozPrikaz(prOdemkni);
        seznPrikazu.vlozPrikaz(prZabi);
        seznPrikazu.vlozPrikaz(prJdi);
        
        
        assertEquals(prKonec, seznPrikazu.vratPrikaz("koniec"));
        assertEquals(prJdi, seznPrikazu.vratPrikaz("jdi"));
        assertEquals(null, seznPrikazu.vratPrikaz("nápoveda"));
        assertEquals(prNacti, seznPrikazu.vratPrikaz("nacti"));
        assertEquals(prSeber, seznPrikazu.vratPrikaz("zober"));
        assertEquals(prBatoh, seznPrikazu.vratPrikaz("batoh"));
        assertEquals(prOdemkni, seznPrikazu.vratPrikaz("odomkni"));
        assertEquals(prZabi, seznPrikazu.vratPrikaz("zabi"));
        
    }
    @Test
    public void testJePlatnyPrikaz() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prUloz);
        seznPrikazu.vlozPrikaz(prNacti);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prBatoh);
        seznPrikazu.vlozPrikaz(prOdemkni);
        seznPrikazu.vlozPrikaz(prZabi);
        seznPrikazu.vlozPrikaz(prJdi);
        
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("koniec"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("jdi"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("nápoveda"));
        assertEquals(prUloz, seznPrikazu.vratPrikaz("uloz"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("nacti"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("zober"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("batoh"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("odomkni"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("zabi"));

    }
    
    @Test
    public void testNazvyPrikazu() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        String nazvy = seznPrikazu.vratNazvyPrikazu();
        assertEquals(true, nazvy.contains("koniec"));
        assertEquals(true, nazvy.contains("jdi"));
        assertEquals(false, nazvy.contains("nápoveda"));
        assertEquals(false, nazvy.contains("Konec"));
    }
    
}
