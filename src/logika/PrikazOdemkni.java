package logika;

/**
 *  Třída PrikazOdemkni implementuje pro hru příkaz odemkni.
 *  
 */
public class PrikazOdemkni implements IPrikaz {
    private static final String NAZEV = "odomkni";
    private HerniPlan plan;

    /***************************************************************************
     *  Konstruktor 
     *  
     *  @param plán herný plán, v ktorom hráč môže odomykať veci
     */ 
    public PrikazOdemkni(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     *  vykoná príkaz "odomkni"
     *  ak sa nezadá parameter, vypíše sa chybové hlásenie v konzole
     *  ak sa východ do zamknutej miestnosti v aktuálnej miestnosti nenechádza, vypíše sa chybové hlásenie v konzole
     *  ak sa nám podarí odomknúť miestnosť, objaví sa hlásenie v konzole, následne môžme zadať príkaz "choď" do danej meistnosti
     *  ak nemáme kľúč, oznámi nám to konzola
     *  ak sme už miestnoť raz odomkli, konzola oznámi že sa tak stalo
     *
     *@param parametry - jako  parametr obsahuje jméno místnosti,
     *                         ktorú chceme odomknúť
     *@return zpráva, kterou vypíše hra hráči
     */ 
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední místnost), tak ....
            return "Čo mám odomknúť? Musíš zadať meno miestnosti";
        }

        String mistnost = parametry[0];

        // hledám zadanou místnost mezi východy
        Prostor sousedniMistnost = plan.getAktualniProstor().vratSousedniProstor(mistnost);

        if (sousedniMistnost == null) {
            System.out.println("Null mistnost");
            return "Odtiaľ nevedú dvere do miestnosti "+mistnost+" !";
        }
        else {
            if (sousedniMistnost.isZamknuta()) {
                System.out.println("Je zamknuta");
                if (plan.getBatoh().obsahujeVec(sousedniMistnost.getKluc().getNazev())) {
                    System.out.println("Odomikam");
                    sousedniMistnost.setZamknuta(false);
                    return "Podarilo sa ti otvoriť dvere do miestnosti "
                           + mistnost + ". Teraz je cesta voľná.";
                }
                else {
                    return "Pre odomknutie dverí do "+mistnost+" potrebuješ mať "
                        + "pri sebe kľúč !";//+ mistnost.getKluc().getNazev();
                }
            }
            else {
                return "Miestnosť "+mistnost+" už bola odomknutá!!!";
            }
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    public String getNazev() {
        return NAZEV;
    }

}