/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;



/*******************************************************************************
 * Instance třídy Vec představují veci, ktoré sa nachádzajú v priestoroch adventúry, 
 * v iných veciach a u postáv.
 *
 * @author    Michal Pénzeš
 */
public class Vec implements IVec
{
    private String nazev;
    private boolean prenositelna;
    
    
    /**
     *  Vytvára jednotlivé veci.Pomenúva ich a určje ich prenositeľnosť podľa parametrov. Zároveň 
     *  založí mapu pre prípadné veci, ktoré by mohla vec obsahovať (ako chladnična obsahuje zázvor).
     *  
     *  @param názov veci
     *  @param údaj o prenositeľnosti
     */
    public Vec(String nazev, boolean jePrenositelna)
    {        
        this.nazev=nazev;
        this.prenositelna = jePrenositelna;
    }
    
    
    /**
     * geter názvu veci
     */
    @Override
    public String getNazev(){
        return nazev;
    }
    
    
    /**
     * setter názvu veci
     */
    public void setNazev(String nazev){
        this.nazev = nazev;
    }
    
    
    /**
     * booleanovská hodnota prenositeľnosti
     */
    @Override
    public boolean isPrenositelna(){
        return prenositelna;
    }
    
    
    /**
     * nastaví prenositeľnosť veci
     */
    public void setPrenositelna(boolean prenositelna){
        this.prenositelna = prenositelna;
    }
    
    
    /**
     * vracia názov
     */
    @Override
    public String toString(){
        return  getNazev();
    }

    /**
     * Metoda equals pro porovnání dvou veci. Překrývá se metoda equals ze
     * třídy Object.
     * Bližší popis metody equals je u třídy Object.
     *
     * @param o object, který se má porovnávat s aktuálním
     * @return hodnotu true, pokud má zadaný prostor stejný název, jinak false
     */
    @Override
    public boolean equals(Object obj) {
        // porovnáváme zda se nejedná o dva odkazy na stejnou instanci
        if (this == obj) {
            return true;
        }
        // porovnáváme jakého typu je parametr 
        if (!(obj instanceof Vec)) {
            return false;    // pokud parametr není typu Vec, vrátíme false
        }
        // přetypujeme parametr na typ Vec 
        Vec druhy = (Vec) obj;

        //metoda equals třídy java.util.Objects porovná hodnoty obou názvů. 
        //Vrátí true pro stejné názvy a i v případě, že jsou oba názvy null,
        //jinak vrátí false.

        return (java.util.Objects.equals(this.nazev, druhy.nazev));
    }

    /**
     * metoda hashCode vraci ciselny identifikator instance, ktery se pouziva
     * pro optimalizaci ukladani v dynamickych datovych strukturach. Pri
     * prekryti metody equals je potreba prekryt i metodu hashCode. Podrobny
     * popis pravidel pro vytvareni metody hashCode je u metody hashCode ve
     * tride Object
     */
    @Override
    public int hashCode() {
        int vysledek = 3;
        int hashNazvu = java.util.Objects.hashCode(this.nazev);
        vysledek = 37 * vysledek + hashNazvu;
        return vysledek;
    }
}

