package logika;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída HraTest slouží ke komplexnímu otestování
 * třídy Hra
 *
 * @author    Jarmila Pavlíčková, Michal Pénzeš
 * @version  pro školní rok 2016/2017
 */
public class HraTest {
    private Hra hra1;
    private Hra hra2;
    private Hra hra3;

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
        hra1 = new Hra();
        hra2 = new Hra();
        hra3 = new Hra();
    }

    /***************************************************************************
     * Testuje průběh hry, po zavolání každěho příkazu testuje, zda hra končí
     * a v jaké aktuální místnosti se hráč nachází.
     * Při dalším rozšiřování hry doporučujeme testovat i jaké věci nebo osoby
     * jsou v místnosti a jaké věci jsou v batohu hráče.
     * 
     */
    @Test
    public void testPrubehHry() {
        
        //VYHERNA HRA
        assertEquals("Tmavá miestnosť", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi Chodba");
        assertEquals(false, hra1.konecHry());
        assertEquals("Chodba", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi Tunel");
        assertEquals(false, hra1.konecHry());
        assertEquals("Tunel", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("zabi Strazca");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("zober Kľúč");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Chodba");
        assertEquals(false, hra1.konecHry());
        assertEquals("Chodba", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("odomkni Pivnica");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Pivnica");
        assertEquals(false, hra1.konecHry());
        assertEquals("Pivnica", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("zober Kamarát");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Chodba");
        assertEquals(false, hra1.konecHry());
        assertEquals("Chodba", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi Tunel");
        assertEquals(false, hra1.konecHry());
        assertEquals("Tunel", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("odomkni Dvere");
        assertEquals(false, hra1.konecHry());
        hra1.zpracujPrikaz("jdi Dvere");
        assertEquals(true, hra1.konecHry());
        
        //PREHRA
        assertEquals("Tmavá miestnosť", hra2.getHerniPlan().getAktualniProstor().getNazev());
        hra2.zpracujPrikaz("jdi Chodba");
        assertEquals(false, hra2.konecHry());
        assertEquals("Chodba", hra2.getHerniPlan().getAktualniProstor().getNazev());
        hra2.zpracujPrikaz("jdi Tunel");
        assertEquals(false, hra2.konecHry());
        assertEquals("Tunel", hra2.getHerniPlan().getAktualniProstor().getNazev());
        hra2.zpracujPrikaz("zabi Strazca");
        assertEquals(false, hra2.konecHry());
        hra2.zpracujPrikaz("zober Kľúč");
        assertEquals(false, hra2.konecHry());
        hra2.zpracujPrikaz("odomkni Dvere");
        assertEquals(false, hra2.konecHry());
        hra2.zpracujPrikaz("jdi Dvere");
        assertEquals(true, hra2.konecHry());
        
        //PRERUSENA HRA
        assertEquals("Tmavá miestnosť", hra3.getHerniPlan().getAktualniProstor().getNazev());
        hra3.zpracujPrikaz("jdi Chodba");
        assertEquals(false, hra3.konecHry());
        assertEquals("Chodba", hra3.getHerniPlan().getAktualniProstor().getNazev());
        hra3.zpracujPrikaz("jdi Tunel");
        assertEquals(false, hra3.konecHry());
        assertEquals("Tunel", hra3.getHerniPlan().getAktualniProstor().getNazev());
        hra3.zpracujPrikaz("zabi Strazca");
        assertEquals(false, hra3.konecHry());
        hra3.zpracujPrikaz("zober Kľúč");
        assertEquals(false, hra3.konecHry());
        hra3.zpracujPrikaz("jdi Chodba");
        assertEquals(false, hra3.konecHry());
        assertEquals("Chodba", hra3.getHerniPlan().getAktualniProstor().getNazev());
        hra3.zpracujPrikaz("odomkni Pivnica");
        assertEquals(false, hra3.konecHry());
        hra3.zpracujPrikaz("jdi Pivnica");
        assertEquals(false, hra3.konecHry());
        assertEquals("Pivnica", hra3.getHerniPlan().getAktualniProstor().getNazev());
        hra3.zpracujPrikaz("koniec");
        assertEquals(true, hra3.konecHry());
    }
}
