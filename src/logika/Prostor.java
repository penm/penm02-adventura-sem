
package logika;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.List;

/**
 * Trida Prostor - popisuje jednotlivé prostory (místnosti) hry
 *
 * Tato třída je součástí jednoduché textové hry.
 *
 * "Prostor" reprezentuje jedno místo (místnost, prostor, ..) ve scénáři hry.
 * Prostor může mít sousední prostory připojené přes východy. Pro každý východ
 * si prostor ukládá odkaz na sousedící prostor.
 *
 * @author Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 * @version pro školní rok 2016/2017
 */
public class Prostor {
    private Postava postava;
    private Truhla truhla;
    private String nazev;
    private String popis;
    private boolean zamok;
    private Set<Prostor> vychody;   // obsahuje sousední místnosti
    private Map<String, Object> veci;
    // private Map<String, Truhla> truhly;
    // private Map<String, Postava> postavy;

    /**
     * Vytvoření prostoru se zadaným popisem, např. "kuchyň", "hala", "trávník
     * před domem"
     *
     * @param nazev nazev prostoru, jednoznačný identifikátor, jedno slovo nebo
     * víceslovný název bez mezer.
     * @param popis Popis prostoru.
     */
    public Prostor(String nazev, String popis) {
        this.nazev = nazev;
        this.popis = popis;
        this.vychody = new HashSet<>();
        this.veci = new HashMap<>();
        this.zamok = false;
        this.truhla = null;
        this.postava = null;
        //this.truhly = new HashMap<>();
    }

    /**
     * Definuje východ z prostoru (sousední/vedlejsi prostor). Vzhledem k tomu,
     * že je použit Set pro uložení východů, může být sousední prostor uveden
     * pouze jednou (tj. nelze mít dvoje dveře do stejné sousední místnosti).
     * Druhé zadání stejného prostoru tiše přepíše předchozí zadání (neobjeví se
     * žádné chybové hlášení). Lze zadat též cestu ze do sebe sama.
     *
     * @param vedlejsi prostor, který sousedi s aktualnim prostorem.
     *
     */
    public void setVychod(Prostor vedlejsi) {
        vychody.add(vedlejsi);
    }

    /**
     * Metoda equals pro porovnání dvou prostorů. Překrývá se metoda equals ze
     * třídy Object. Dva prostory jsou shodné, pokud mají stejný název. Tato
     * metoda je důležitá z hlediska správného fungování seznamu východů (Set).
     *
     * Bližší popis metody equals je u třídy Object.
     *
     * @param o object, který se má porovnávat s aktuálním
     * @return hodnotu true, pokud má zadaný prostor stejný název, jinak false
     */  
      @Override
    public boolean equals(Object o) {
        // porovnáváme zda se nejedná o dva odkazy na stejnou instanci
        if (this == o) {
            return true;
        }
        // porovnáváme jakého typu je parametr 
        if (!(o instanceof Prostor)) {
            return false;    // pokud parametr není typu Prostor, vrátíme false
        }
        // přetypujeme parametr na typ Prostor 
        Prostor druhy = (Prostor) o;

        //metoda equals třídy java.util.Objects porovná hodnoty obou názvů. 
        //Vrátí true pro stejné názvy a i v případě, že jsou oba názvy null,
        //jinak vrátí false.

       return (java.util.Objects.equals(this.nazev, druhy.nazev));       
    }

    /**
     * metoda hashCode vraci ciselny identifikator instance, ktery se pouziva
     * pro optimalizaci ukladani v dynamickych datovych strukturach. Pri
     * prekryti metody equals je potreba prekryt i metodu hashCode. Podrobny
     * popis pravidel pro vytvareni metody hashCode je u metody hashCode ve
     * tride Object
     */
    @Override
    public int hashCode() {
        int vysledek = 3;
        int hashNazvu = java.util.Objects.hashCode(this.nazev);
        vysledek = 37 * vysledek + hashNazvu;
        return vysledek;
    }
      

    /**
     * Vrací název prostoru (byl zadán při vytváření prostoru jako parametr
     * konstruktoru)
     *
     * @return název prostoru
     */
    public String getNazev() {
        return nazev;       
    }

    /**
     * Vrací "dlouhý" popis prostoru, který může vypadat následovně: Jsi v
     * mistnosti/prostoru vstupni hala budovy VSE na Jiznim meste. vychody:
     * chodba bufet ucebna
     *
     * @return Dlouhý popis prostoru
     */
    public String dlouhyPopis() {
        String tru = "";
        String ve = ""; 
        String p = "";
            for (Object vec : veci.values()){
                    if(vec instanceof Vec) {    
                    
                    ve +=  vec.toString() + " ";
                    
                }
            }
            
                if(truhla != null){
                       
                        tru = "Je tu trezor";
                }
                
                if(postava != null){
                       
                        p = "Je tu " + postava.getJmeno();
                }
            
        String result = "Si v miestnosti. " + popis + ".\n"
         + popisVychodu() +".\n" + "Nachádza sa tu: "+ ve + " \n" + tru  + p;     
                
        return result;
    }

    /**
     * Vrací textový řetězec, který popisuje sousední východy, například:
     * "vychody: hala ".
     *
     * @return Popis východů - názvů sousedních prostorů
     */
    private String popisVychodu() {
        String vracenyText = "Východy: ";
        for (Prostor sousedni : vychody) {
            vracenyText += sousedni.getNazev() + " ";
        }
        return vracenyText;
    }

    /**
     * Vrací prostor, který sousedí s aktuálním prostorem a jehož název je zadán
     * jako parametr. Pokud prostor s udaným jménem nesousedí s aktuálním
     * prostorem, vrací se hodnota null.
     *
     * @param nazevSouseda Jméno sousedního prostoru (východu)
     * @return Prostor, který se nachází za příslušným východem, nebo hodnota
     * null, pokud prostor zadaného jména není sousedem.
     */
    public Prostor vratSousedniProstor(String nazevSouseda) {
        List<Prostor>hledaneProstory = 
            vychody.stream()
                   .filter(sousedni -> sousedni.getNazev().equals(nazevSouseda))
                   .collect(Collectors.toList());
        if(hledaneProstory.isEmpty()){
            return null;
        }
        else {
            return hledaneProstory.get(0);
        }
    }

    /**
     * Vrací kolekci obsahující prostory, se kterými tento prostor sousedí.
     * Takto získaný seznam sousedních prostor nelze upravovat (přidávat,
     * odebírat východy) protože z hlediska správného návrhu je to plně
     * záležitostí třídy Prostor.
     *
     * @return Nemodifikovatelná kolekce prostorů (východů), se kterými tento
     * prostor sousedí.
     */
    public Collection<Prostor> getVychody() {
        return Collections.unmodifiableCollection(vychody);
    }
    
    
    /**
     * vkladá vec do priestoru
     * 
     * @param vkladaná vec
     */
    public void vlozVec(Vec vec){
        this.veci.put(vec.getNazev(), vec);
    }
    
    
    /**
     * vkladá vec do truhly
     * 
     * @param vkladaná vec
     */
    public void vlozVec(Truhla truhla){
        this.veci.put(truhla.getNazev(), truhla);
    }
    
    
    /**
     * vymaže vec
     * 
     * @param vec ktorú chceme vymazať
     */
    public void vymazVec(Vec vec){
        this.veci.remove(vec.getNazev(), vec);
    }
    
    /**
     * vypíše veci v priestore
     */
    public String veciVProstore(){
        String result = "Nachádza sa tu: ";
        for(String vec : veci.keySet()){
            result +=vec + " ";
        }
        return result;
    }
    
    
    /**
     * vypíše truhlice v preistore
     */
    public String truhlyVProstore(){
        String result = "Trezory: ";
        for(String truhla : veci.keySet()){
            result += truhla + " ";
        }
        return result;
    }    
    
    
    /**
     * vypíše postavy v priestore
     */
    public String postavyVProstore(){
        String result = "Postavy: ";
        for(String pos : veci.keySet()){
            result += pos + " ";
        }
        return result;
    }
    
    
    /**
     * vyberie vec
     */
    public Vec vyberVec(String nazev){
        Vec vec;
        if (veci.containsKey(nazev)){
         vec = (Vec) veci.get(nazev);
         return vec;
        }
        return null;
    }
    
    
    /**
     * vyberie veci z truhlice
     */
    public Set<Vec> vyberTruhlu(){
        if (truhla != null){
         return truhla.getVeci();
        }
        return null;
    }

    
    /**
     * potreba kľúču ak sú dvere zamknuté
     */
    public Vec getKluc() {
        if (zamok) {
            return new Vec("Kľúč", true);
        }
        
        return null;
    }
    
    
    /**
     * geter postavy
     */
    public Postava getPostava() {
        return postava;
    }
    
    
    /**
     * seter psotavy
     */
    public void setPostava(Postava postava) {
        this.postava = postava;
    }
   
    
    /**
     * seter truhly
     */
    public void setTruhla(Truhla truhla) {
        this.truhla = truhla;
    }
     
    
    /**
     * metóda ktorá definuje či je niečo v hre zamknuté
     */
    public void setZamknuta(boolean zamok){
        this.zamok = zamok;
    }
    
    
    /**
     * motóda ktorá nám povie že je vec zamknutá
     */
    public boolean isZamknuta(){
        return zamok;
    }

    @Override
    public String toString(){ return getNazev(); }


}
