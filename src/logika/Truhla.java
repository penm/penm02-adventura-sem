/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;

/*******************************************************************************
 * Instancia triedy truhla
 *
 * @author    Michal Pénzeš
 * @version   0.00.000
 */
public class Truhla extends AbstractUloziste implements IVec
{
    private int maximalniVelikost;
    private String nazev;
    
    
    /**
     * nastaví truhlu v priestore
     */
    public Truhla(String nazev, int maximalniVelikost){
        super();
        this.maximalniVelikost = maximalniVelikost;
        this.nazev = nazev;
    }
    
    
    /**
     * seter pre veci z truhly
     */
    public Set<Vec> getVeci() {
        return this.veci;
    }
    
    
    /**
     * zsti či je možné vložiť vec
     * @param vec
     */
    public boolean mohuVlozit(Vec vec){
        return pocetVeci() < maximalniVelikost;
    }
    
    
    /**
     * definuje či je truhlica prenositeľná (či ju môže hráč zobrať do batohu)
     */
    public boolean isPrenositelna(){
        return false;
    }
    
    
    /**
     * vracia názov truhlice
     */
    public String getNazev(){
        return nazev;
    }
    
    /**
     * Metoda equals pro porovnání dvou veci. Překrývá se metoda equals ze
     * třídy Object.
     * Bližší popis metody equals je u třídy Object.
     *
     * @param o object, který se má porovnávat s aktuálním
     * @return hodnotu true, pokud má zadaný prostor stejný název, jinak false
     */
    @Override
    public boolean equals(Object o) {
        // porovnáváme zda se nejedná o dva odkazy na stejnou instanci
        if (this == o) {
            return true;
        }
        // porovnáváme jakého typu je parametr 
        if (!(o instanceof Truhla)) {
            return false;    // pokud parametr není typu Vec, vrátíme false
        }
        // přetypujeme parametr na typ Vec 
        Truhla druhy = (Truhla) o;

        //metoda equals třídy java.util.Objects porovná hodnoty obou názvů. 
        //Vrátí true pro stejné názvy a i v případě, že jsou oba názvy null,
        //jinak vrátí false.

        return (java.util.Objects.equals(this.nazev, druhy.nazev));
    }

    /**
     * metoda hashCode vraci ciselny identifikator instance, ktery se pouziva
     * pro optimalizaci ukladani v dynamickych datovych strukturach. Pri
     * prekryti metody equals je potreba prekryt i metodu hashCode. Podrobny
     * popis pravidel pro vytvareni metody hashCode je u metody hashCode ve
     * tride Object
     */
    @Override
    public int hashCode() {
        int vysledek = 3;
        int hashNazvu = java.util.Objects.hashCode(this.nazev);
        vysledek = 37 * vysledek + hashNazvu;
        return vysledek;
    }
}


