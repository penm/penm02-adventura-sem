package logika;

import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída ProstorTest slouží ke komplexnímu otestování
 * třídy Prostor
 *
 * @author    Michal Pénzeš
 * @version   1.
 */
public class ProstorTest
{

    /**
     * Testuje, ci su spravne nastaveny průchody mezi prostory hry. Prostory
     * nemusí odpovídat vlastní hře, 
     */
    @Test
    public  void testLzeProjit() {      
       Prostor Chodba = new Prostor("Chodba", "Vchádzaš do dlhej tmavej chodby.");
       Prostor Pracovna = new Prostor("Pracovňa","Vošiel si do dverí na konci chodby. Nachádzaš sa v pracovni. Nikto tu nieje. Na stole sa nachádza počítač.");
       
       Chodba.setVychod(Pracovna);
       Pracovna.setVychod(Chodba);
       
       assertEquals(Chodba, Pracovna.vratSousedniProstor("Chodba"));
       assertEquals(null, Chodba.vratSousedniProstor("Pracovna"));
        
        
       
    }
    /**
     * Testuje ci sa daju zobrat predmety
     */
    @Test
    public void testVec() {
        Vec Motorka = new Vec("Motorka", true);
        Vec kniha = new Vec("KnihaProgramovanieVJave", true);
        Prostor Sklad  = new Prostor("Sklad", "je tu motorka a kniha");
        Sklad.vlozVec(Motorka);
        Sklad.vlozVec(kniha);
        
        assertEquals(Motorka, Sklad.vyberVec("Motorka"));
        assertEquals(kniha, Sklad.vyberVec("KnihaProgramovanieVJave"));
        assertEquals(null, Sklad.vyberVec("neexistujucaVec"));
        
    }
    /**
     * Test prenositelnosti
     */
    @Test
    public void testPrenositelnosti() {
    Vec vec1 = new Vec("Vec", true);
    assertEquals(true, vec1.isPrenositelna());
    vec1.setPrenositelna(false);
    assertEquals(false, vec1.isPrenositelna());
    
    }

   
    
}