package logika;

import java.util.Set;
import java.util.HashSet;
/**
 * Predstavuje postavy vyskytujúce sa v miestnostiach hry.
 *
 * @author (Michal Pénzeš)
 */
public class Postava
{
    private String jmeno;
    private Set<Vec> batoh;
    private boolean jeMrtva;
    
    
    /**
     * metóda definuje meno postavy, ktorá sa vyskytuje v hre, defaultne je nastavená že žije
     * postava môže mať pri sebe vec
     */
    public Postava(String jmeno, Set veci)
    {        
        this.jmeno = jmeno;
        this.jeMrtva = false;
        this.batoh = new HashSet<>(veci);
    }

    
    /**
     * vypíše veci ktoré mala postava pri sebe ak sme ju zabili
     */
    public void vypisVeci() {
        if (jeMrtva) {
            System.out.println("Batoh:");
            for (Vec vec : batoh) {
                System.out.println(vec.getNazev());
            }
        } 
    }

    
    /**
     * ak je postava mrtva tak vec vyhodí na zem
     */
    public Vec zoberVec(String nazovVeci) {
        if (jeMrtva) {
            for (Vec vec : batoh) {
                if (vec.getNazev().equals(nazovVeci)) {
                    Vec toRet = vec;
                    batoh.remove(vec);
                    return toRet;
                }    
            }
        }

        // Nie je mrtva
        return null;
    }
    
    
    /**
     * veci, ktoré má postava pri sebe
     */
    public Set<Vec> getVeci() {
        return batoh;
    }

    
    /**
     * pri naísaní príkazu zabi postava zomrie
     */
    public void zabi() {
        jeMrtva = true;
    }
    
    
    /**
     * meno postavy
     */
    public String getJmeno(){
        return jmeno;
    }
    
   
}
