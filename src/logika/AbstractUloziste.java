/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;

import java.util.Collection;
import java.util.Set;
import java.util.HashSet;

/*******************************************************************************
* Abstraktna trieda ulozisko
 * Zistí či vložená vec je "bonusovíBodík" a ak je tam vypíse na konzolu informáciu o získani bonusových bodov za semestrálku.
 * @author    Michal Pénzeš
 * @version   1.00
 */
public abstract class AbstractUloziste
{   
   protected Set<Vec> veci = new HashSet<Vec>();
   /**
    * Metóda vkladá veci do batoha
    * Pri vložení do konzoly vypíše príslušný text
    */
   public void vloz(Vec vec){
       if (vec.getNazev().equals("KnihaProgramovanieVJave")){
       this.veci.add(vec);
       System.out.println("Vzal si " + vec.getNazev());
        }
        if (mohuVlozit(vec)){
       this.veci.add(vec);
       System.out.println("Vlozil si " + vec.getNazev()) ;
  }
}
  /**
   * Metoda pre vlozenie veci do truhly
   * @param vec
   */
  public void vlozTruhly(Vec vec){
       this.veci.add(vec);
    
  }
  /**
   * Metoda pre vypisanie veci
   * @return vratVec - vrati vec
   */
   public String vypisVeci() {
       String vratVec = "Veci: ";
        for (Vec vec : veci) {
            vratVec += vec.getNazev() + ", ";
        }
        return vratVec;
    }
   /**
    * Metoda pre zistenie poctu veci
    * return - vrati pocez veci
    */
   public int pocetVeci(){
       return this.veci.size();
   }
   /**
    * Metoda pre vybranie veci z uloziska
    * @param nazovVeci
    * @return vratVec - vrati vec
    */
    public Vec vyberVec(String nazovVeci) {
        for (Vec vec : veci) {
            if (vec.getNazev().equals(nazovVeci)) {
                Vec vratVec = vec;
                veci.remove(vec);
                return vratVec;
            }    
        }
        
        return null;
    }
    /**
     * Ci obsahuje danu vec, zisti nazov veci a potom ho porovna pomocou equals.
     * @param String nazovVeci - nazov konkretnej veci
     */
    public boolean obsahujeVec(String nazovVeci) {
        for (Vec vec : veci) {
            if (vec.getNazev().equals(nazovVeci)) {
              return true;
            }    
        }

        return false;
    }
    /**
     * Ci sa da vlozit, alebo nie
     * @param Vec vec - 
     */
    public abstract boolean mohuVlozit(Vec vec);

    public Collection<Vec> getSeznamVeci(){
        return veci;
    }


}