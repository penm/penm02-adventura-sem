package logika;

import java.util.Set;

/**
 * Instancia triedy PrikazSeber implementujú príkaz zober pre hru
 *
 * @author (Michal Pénzeš)
 */
public class PrikazSeber implements IPrikaz
{
    private static final String NAZEV = "zober";
    private HerniPlan plan;
    private Batoh batoh;
    
    /***************************************************************************
     *  Konstruktor 
     *  
     *  @param plan herný plán
     */
    public PrikazSeber(HerniPlan plan, Batoh batoh){
    this.plan = plan;
    this.batoh = batoh;
    }
  
    /**
     *  Vykoná príkaz "zober".
     *  Ak užívateľ nedodá parameter čo má zobrať vypíše sa to v konzole.
     *  Ak vec v priestore nieje, vypíše chybové hlásenie do konzoly. 
     *  Ak sa vec zobrať nedá, vypíše sa to v konzole
     *
     *@param parametry - parametrom je názov veci,ktorú chceme zobrať
     *@return správa, ktorú vypíše hra hráčovi
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0){
            return "Čo mám zobrať ?";
        }

        String nazev = parametry[0];        
        Prostor aktualniProstor = plan.getAktualniProstor();
        
        
        if (nazev.equals("Trezor")) {
            Set<Vec> veci = aktualniProstor.vyberTruhlu();
            for (Vec vec : veci) {
                batoh.vloz(vec);
            }
            aktualniProstor.setTruhla(null);
            
            return batoh.vypisVeci();
        } 
        
        Vec vec = aktualniProstor.vyberVec(nazev);
        
        if (vec == null){
            return "Nič tu nieje";
        
        }else if (vec.isPrenositelna()){            
        batoh.vloz(vec);
        aktualniProstor.vymazVec(vec);
        return batoh.vypisVeci();
    }else{
        return "Nedá sa zobrat";
    }
             
        
    }
    
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}

