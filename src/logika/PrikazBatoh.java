package logika;

/**
 *  Třída PrikazBatoh implementuje pro hru naše úložisko v podobe batohu, do ktorého môžeme 
 *  zbierať veci
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 *  
 */

class PrikazBatoh implements IPrikaz {

    private static final String NAZEV = "batoh";
    private Batoh batoh;

    /**
     *  Konstruktor třídy
     */    
    public PrikazBatoh(Batoh batoh) {
        this.batoh = batoh;
    }

    /**
     * Vypíše veci ktoré obsahuje batoh.
     * V prípade že hráč zadá okrem slova "batoh" viac slov vypíše sa v konzole hlásenie
     */

    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length > 0) {
            return "Stačí napísať slovo 'batoh'";
        }
        else {
            return batoh.vypisVeci();
            
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}
