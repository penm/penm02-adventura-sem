/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import main.MainController;
import uiText.TextoveRozhrani;


/*******************************************************************************
 * Třída  Start je hlavní třídou projektu,
 * který představuje jednoduchou textovou adventuru určenou k dalším úpravám a rozšiřování
 *
 * @author    Jarmila Pavlíčková
 * @version   ZS 2016/2017
 */
public class Start extends Application
{

    public static IHra hra;
    public static FXMLLoader loader;

    /***************************************************************************
     * Metoda, prostřednictvím níž se spouští celá aplikace.
     *
     * @param args Parametry příkazového řádku
     */
    public static void main(String[] args)
    {
        if (args.length > 0) {
            if (args[0].equals("text")) {
                IHra hra = new Hra();
                TextoveRozhrani ui = new TextoveRozhrani(hra);
                ui.hraj();
            } else {
                System.out.println("Neplatný parametr. Zadejte parametr text pro textovou verzi.");
            }
        } else {
            launch(args);
        }

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/resources/main.fxml"));
        GridPane root = loader.load();
        MainController controller = loader.getController();

        primaryStage.setTitle("Adventura");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

        IHra hra = new Hra();
        controller.inicializuj(hra);
    }

    //metoda ktora sa volá na začatie novej hry
    public static void startHry() {

        // spustime si novu inštanciu hry, to same ako v metode start
        loader.setLocation(Start.class.getResource("/main.fxml"));
        MainController c = loader.getController();
        hra = new Hra();
        c.inicializuj(hra);
    }

    //metoda odkazuje na metodu startHry()
    public static void newHra() {
        try {

            startHry();

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    //metoda vytvori novy stage, na nom novu scenu na ktorej spusti fxml šablonu Help.fxml (podobne ako v metode start)
    public static void startHelp() {
        try {
            Stage helpStage = new Stage();
            FXMLLoader helpLoader = new FXMLLoader();
            helpLoader.setLocation(Start.class.getResource("/resources/help.fxml"));

            Parent helpRoot = helpLoader.load();
            Scene scene = new Scene(helpRoot, 800, 600);
            helpStage.setScene(scene);
            helpStage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }



}
