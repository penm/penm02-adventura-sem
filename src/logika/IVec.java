/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;



/*******************************************************************************
 * interface triedy Vec
 *
 * @author    Michal Pénzeš
 */
public interface IVec
{
   public String getNazev();
   public boolean isPrenositelna();
}
