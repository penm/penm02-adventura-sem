/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;


import java.util.Collection;
import java.util.Map;

/*******************************************************************************
 * Definovanie batohu a jeho kapacity stanovenej na 5 položiek, táto trieda implementuje uložište
 *
 * @author    Michal Pénzeš
 * @version   0.00.000
 */
public class Batoh extends AbstractUloziste
{
    //private Map<String, Vec> seznamVeci;
    private static final int KAPACITA = 5;
    private static Batoh instance;
    
    
    /**
     * metóda volá konštruktor "rodiča"
     */
    private Batoh(){
       // super();
    }
    
    
    /**
     * Vytvorí novú inštanciu batoh, ak neexistuje
     * returnuje samotnú inštanciu
     */
    public static Batoh getInstance(){
        if (instance == null){
            instance = new Batoh();
        }
        return instance;
    }  
    
    /**
     * Zisťuje či ešte môže vložiť ďalši predmet do batohu. Vloži ho len ak počet predmetov nepresiahol kapacitu batohu, ktorá je pevne daná na 5 predmetov.
     * @param Vec vec - vec ktora sa ide vlozit
     */
    public boolean mohuVlozit(Vec vec) {
        return vec.isPrenositelna() && this.pocetVeci() < KAPACITA;
    }

    /**public Collection<Vec> getSeznamVeci() {
        return seznamVeci.values();
    }**/
}
