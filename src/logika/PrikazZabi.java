package logika;

import java.util.Set;

public class PrikazZabi implements IPrikaz {
    private static final String NAZEV = "zabi";
    private HerniPlan plan;

    /**
    *  Konštruktor triedy
    * 
    */    
    public PrikazZabi(HerniPlan plan) {
        this.plan = plan;
    }

  /**
     *  Príkaz na zabitie postavy.
     *  Ak užívateľ zadá iba príkaz zabi a nepriradí mu čo má zabiť tak sa vypíše "Čo" a užívateľ musí doplniť čo chce zabiť.
     *  Ak nieje v miestnosti žiadna postava, vypíše sa "Takáto postava tu nie je!".
     *
     *@param parametre - parametrom je názov postavy, ktorú chceme zneškodniť.
     *@return správa, ktorú vypíše hra hráčovi
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Koho?";
        }



        Prostor aktualniProstor = plan.getAktualniProstor();

        if (aktualniProstor.getPostava() == null) {
            return "Takáto postava tu nie je!";
        }
            Set<Vec> veci = aktualniProstor.getPostava().getVeci();

            for (Vec vec : veci) {
                aktualniProstor.vlozVec(vec);
            }

            aktualniProstor.setPostava(null);

            return aktualniProstor.veciVProstore();
    }
    
    /**
     *  Metoda vracia názov príkazu (slovo ktoré používá hráč pre jeho vyvolání)
     *  
     *  @ return názov príkazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}