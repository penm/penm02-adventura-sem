package logika;



import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Test triedy batoh.
 *
 * @author  Michal Pénzeš
 * @version (a version number or a date)
 */
public class BatohTest
{
    private Batoh batoh;
   
    
    
    @Test
    public void testBatohu(){
            batoh = batoh.getInstance();
            Vec vec1 = new Vec("1",true);
            Vec vec2 = new Vec("2",true);
            Vec vec3 = new Vec("3",true);
            Vec vec4 = new Vec("4",true);
            Vec vec5 = new Vec("5",true);
            Vec vec6 = new Vec("6",true);
            Vec vec7 = new Vec("7", false);
            batoh.vloz(vec1);
            batoh.vloz(vec2);
            batoh.vloz(vec3);
            batoh.vloz(vec4);
            batoh.vloz(vec5);
            batoh.vloz(vec6);
            batoh.vloz(vec7);
           
            
            assertEquals(true, batoh.obsahujeVec("1"));
            assertEquals(true, batoh.obsahujeVec("2"));
            assertEquals(true, batoh.obsahujeVec("3"));
            assertEquals(true, batoh.obsahujeVec("4"));
            assertEquals(true, batoh.obsahujeVec("5"));
            assertEquals(false, batoh.obsahujeVec("6"));
            assertEquals(false, batoh.obsahujeVec("7"));
    }
}
