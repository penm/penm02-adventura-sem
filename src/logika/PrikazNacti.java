package logika;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


/**
 * Write a description of class PrikazNacti here.
 *
 * @author (Michal Pénzeš)
 */
public class PrikazNacti implements IPrikaz {

    private static final String NAZEV = "nacti";
    private Hra hra;

    /**
     * V případě, že příkaz má jen jedno slovo "konec" hra končí(volá se metoda setKonecHry(true))
     * jinak pokračuje např. při zadání "konec a".
     * 
     * @return zpráva, kterou vypíše hra hráči
     */
    
    public PrikazNacti(Hra hra) {
        this.hra = hra;
        
    }
    
    
    /**
     * načíta hru z textového súboru a hra nás uvíta po načítaní
     */
    @Override
    public String provedPrikaz(String... parametry) {
        
        if (parametry.length > 0) {
            return ".....";
        }
        else{
            nacti("logika/Subory/Output.txt");
            return ("Načítal si hru .\n") + hra.vratUvitaniPoNacteni() ;
        }                
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }   
    
    
    /**
     * načíta uložené príkazy v textovom súbore
     */
    public String nacti(String filename) {
        try (BufferedReader brr = new BufferedReader(new FileReader(filename))) {

            String radek;
                        
            while ((radek = brr.readLine()) != null) {
                hra.zpracujPrikaz(radek);
            }
                   
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

