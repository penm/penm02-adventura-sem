package logika;

import main.Observer;
import main.Subject;

import java.util.HashSet;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;


/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan implements Subject {
    
    private Prostor aktualniProstor;
    private Batoh batoh;
    private List<Observer> seznamPozorovatelu;



    /**
     * geter batohu
     */
    public Batoh getBatoh() {
        return batoh;
    }
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan(Batoh batoh) {
        this.batoh = batoh;
        zalozProstoryHry();

        seznamPozorovatelu = new ArrayList<>();

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor tmavaMiestnost = new Prostor("Tmavá miestnosť","Prebúdzaš sa v tmavej miestnosti. Si dezorientovaný a musíš sa dostať na slobodu.");
        Prostor Chodba = new Prostor("Chodba", "Vchádzaš do dlhej tmavej chodby.");
        Prostor Pracovna = new Prostor("Pracovňa","Vošiel si do dverí na konci chodby. Nachádzaš sa v pracovni. Nikto tu nieje. Na stole sa nachádza počítač.");
        Prostor Pivnica = new Prostor("Pivnica","Stará plesnivá pivnica. Je tu uväznený váš kamarát.");
        Prostor Sklad = new Prostor("Sklad","Je tu obrovský neporiadok. V skrini sa nachádza trezor, na ktorý potrebujete číselný kód.");
        Prostor Tunel = new Prostor("Tunel", "Zišiel si dole tmavým schodiskom. Objavil si dvere. Tie viedli do starého tunela. Na začiatku tunela uvidíš spiaceho stážcu, ktorého môžeš v tichosti zabiť a zobrať mu zbraň. Na konci tunela sa nachádzajú dvere na slobodu, ktoré sú ale zamknuté. Kľúč má strážca pred dverami, ktorého musíš zabiť zbraňou.");
        Prostor Dvere = new Prostor("Dvere", "Sloboda !");
        // přiřazují se průchody mezi prostory (sousedící prostory)
        tmavaMiestnost.setVychod(Chodba);
        Chodba.setVychod(Pracovna);
        Chodba.setVychod(Pivnica);
        Chodba.setVychod(Sklad);
        Chodba.setVychod(Tunel);
        Chodba.setVychod(tmavaMiestnost);
        Pracovna.setVychod(Chodba);
        Pivnica.setVychod(Chodba);
        Sklad.setVychod(Chodba);
        Tunel.setVychod(Chodba);
        Tunel.setVychod(Dvere);
        
        
        //ZAMNKNUTÉ MIESTONSTI        
        Pivnica.setZamknuta(true);
        Dvere.setZamknuta(true);
        
        
        //VECI V MIESTOSTI
        Sklad.vlozVec(new Vec("Motorka", false));
        Pracovna.vlozVec(new Vec("Počítač", false));
        Pivnica.vlozVec(new Vec("Kamarát", true));
        Set veci = new HashSet<>();
        veci.add(new Vec("Kamarát", true));
        Set veciK = new HashSet<>();
        veciK.add(new Vec("Kľúč", true));
        Tunel.setPostava(new Postava("SpiaciStrážca", veciK));
        veciK.add(new Vec("Kľúč", true));
        Truhla truhla = new Truhla("Trezor", 5);
        truhla.vloz(new Vec("KnihaProgramovanieVJave", true));
        Sklad.setTruhla(truhla);

        // hra začíná v tmavej miestnosti 
        aktualniProstor = tmavaMiestnost;     
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {

        aktualniProstor = prostor;
        notifyObservers();

    }

    @Override
    public void register(Observer observer) {
        seznamPozorovatelu.add(observer);
    }

    @Override
    public void unregister(Observer observer) {
        seznamPozorovatelu.remove(observer);

    }

    @Override
    public void notifyObservers() {
        for (Observer pozorovatel : seznamPozorovatelu) {
            pozorovatel.update();
        }

    }


}
