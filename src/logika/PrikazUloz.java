package logika;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

/**
 *  Třída
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 *  
 */

class PrikazUloz implements IPrikaz {

    private static final String NAZEV = "uloz";
    private Hra hra;
    

    /**
     * ak zadáme iba slovo "uloz" hra sa uloží
     * inak pokračuje napr. pri zadání "uloz a".
     * 
     * @return zpráva, kterou vypíše hra hráči
     */
    
    public PrikazUloz(Hra hra) {
        this.hra = hra;
    }

    @Override
    public String provedPrikaz(String... parametry) {
        List<String> pouzitePrikazy;
        pouzitePrikazy = new ArrayList<>();
        
        if (parametry.length > 0) {
            return ".....";
        }
        else{
            pouzitePrikazy = hra.getPouzitePrikazy();
            zapis("logika/Subory/Output.txt",pouzitePrikazy);
            return "hra je uložená";
        }                
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

    /**
     * zapíše použité príkazy do externého súboru a hodí každý príkaz do samostatného riadku
     */
    public static void zapis(String filename, List<String> list) {
        try (BufferedWriter bww = new BufferedWriter(new FileWriter(filename))) {
            
            StringBuilder obsah = new StringBuilder();
            
            for (String s : list) {
                if(!s.equals("nacti")){
                obsah.append(s);
                obsah.append("\n");
            }
        }
            
            bww.write(obsah.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

